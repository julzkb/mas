import unittest
from lxml import html
import requests
import get_artist_links
import get_artist_details

class TestGetArtistLinks(unittest.TestCase):
    def test_pages(self):
        x = get_artist_links.pages('R')
        self.assertGreater(x, 0)

    def test_get_link(self):
        x = get_artist_links.get_links('X')
        self.assertGreater(len(x), 1)


class TestGetArtistDetails(unittest.TestCase):
    def test_get_all_links(self):
        x = get_artist_details.get_all_links()
        self.assertGreater(len(x), 2)

    def test_get_details(self):
        sample_page = 'https://www.metal-archives.com/bands/A_Crime_of_Passion/3540415046'
        page = requests.get(sample_page)
        tree = html.fromstring(page.text)
        x = get_artist_details.get_details(tree)
        self.assertGreater(len(x['artist']), 1)
        self.assertGreater(len(x['country']), 1)
        self.assertGreater(len(x['location']), 1)
        self.assertGreater(len(x['genre']), 1)
        




if __name__ == "__main__":
    unittest.main()

