import csv
import requests
import time
import sys
import logging
import json
import random
from lxml import html
from tqdm import tqdm

LINKS = '../data/artist_links.csv'

logging.basicConfig(level=logging.INFO)


def get_all_links():
    '''
    Gets all the artists links we scraped previously:
    Input : None
    Returns : list of links
    '''

    with open(LINKS, 'r') as artist_links_csv:
        reader = csv.reader(artist_links_csv)
        all_links = list(reader)

    artist_links_csv.close()

    return all_links


def get_details(tree):
    '''
    Scrape the detail we want from eachg page
    Input : elemtree
    Returns : Dict with all the detail
    '''

    artist = tree.xpath('//*[@id="band_info"]/h1/a/text()')[0]
    country = tree.xpath('//*[@id="band_stats"]/dl[1]/dd[1]/a/text()')[0]
    location = tree.xpath('//*[@id="band_stats"]/dl[1]/dd[2]/text()')[0]
    status = tree.xpath('//*[@id="band_stats"]/dl[1]/dd[3]/text()')[0]
    formed_in = tree.xpath('//*[@id="band_stats"]/dl[1]/dd[4]/text()')[0]
    years_active = tree.xpath('//*[@id="band_stats"]/dl[3]/dd/text()')[0]
    genre = tree.xpath('//*[@id="band_stats"]/dl[2]/dd[1]/text()')[0]
    lyrical_themes = tree.xpath(
        '//*[@id="band_stats"]/dl[2]/dd[2]/text()')[0]

    results = {'artist': artist,
               'country': country,
               'location': location,
               'status': status,
               'formed_in': formed_in,
               'years_active': years_active,
               'genre': genre,
               'lyrical_themes': lyrical_themes}

    return results


def main():

    all_links = get_all_links()
    n = 0

    # TQDM progress bar
    for i in tqdm(all_links):
        try:
            page = requests.get(i[1])
            tree = html.fromstring(page.text)

            if len(tree.xpath('//*[@id="band_info"]/h1/a/text()')) > 0:
                results = get_details(tree)

                # write results to json file
                with open('../data/output.jl', 'a', encoding='utf-8') as outputFile:
                    json.dump(results, outputFile, ensure_ascii=False)
                    outputFile.write('\n')

                # add to already parsed CSV
                with open('../data/parsed.csv', 'a') as parsedFile:
                    writer = csv.writer(parsedFile)
                    writer.writerow(i)

            else:
                # add to already parsed CSV
                with open('../data/failed.csv', 'a') as failedFile:
                    writer = csv.writer(failedFile)
                    writer.writerow(i)

            # update to parse csv
            all_links.pop(n)
            with open(LINKS, 'w', newline='') as artist_links_csv:
                writer = csv.writer(artist_links_csv)
                writer.writerows(all_links)

            n = n+1
            # let's randomise the sleeping a bit
            time.sleep(random.randrange(2, 7))

        except requests.RequestException as e:
            logging.error(e)
            sys.exit(1)


if __name__ == "__main__":
    main()
