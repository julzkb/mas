import csv
import requests
import json
import time
import math
import logging
from string import ascii_uppercase

logging.basicConfig(level=logging.INFO)
TOTAL_RECORDS = '../data/total_records.json'

HEADERS = {
    'User-Agent': "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0"}


def pages(letter):
    '''
    Calculates how many pages there are based on total records ( steps of 500 )
    Returns : int 
    '''
    with open(TOTAL_RECORDS, 'r') as total_records:
        records = json.load(total_records)[letter]

    return math.ceil(records/500)


def get_links(letter):
    '''
    Get all artists , links 
    Input: Letter 
    Returns: List of artists, links
    '''
    n = 0
    results = []

    for i in range(0, pages(letter)+1):
        base_url = "https://metal-archives.com/browse/ajax-letter/l/{}/json/1?sEcho=1&iColumns=4&sColumns=&iDisplayStart={}&iDisplayLenght={}".format(
            letter, n, n+500)
        response = requests.get(base_url, headers=HEADERS)
        json_data = json.loads(response.text)

        for i in json_data['aaData']:
            link = i[0].split('a href=')[1].split('>')[0].replace("'", "")

            artist = i[0].split('>')[1].split('<')[0]
            results.append((artist, link))

        n = n+500
        time.sleep(3)

    return results


def main():

    # we go through the alphabet
    for i in ascii_uppercase:
        with open('../data/artist_links.csv', 'a') as csvFile:
            logging.info(f'Getting : {i}')
            writer = csv.writer(csvFile)
            writer.writerows(get_links(i))


if __name__ == "__main__":
    main()
