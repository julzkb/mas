import json
import sqlite3


def dedup(jsonfile):
    '''
    Deduplicates the input json file
    Input : jsonfile
    Returns : List of dicts
    '''

    lines = []
    with open(jsonfile, 'r') as dataFile:
        for line in dataFile:
            lines.append(json.loads(line))

    dedup_data = [dict(t) for t in {tuple(d.items()) for d in lines}]

    return dedup_data


def clean_data(data):
    '''
    Reformats the input data into tuples with the right format for the DF
    Inputs : list of dict
    Returns : list of clean tuples
    '''

    results = []
    n = 1

    for d in data:
        artist = d['artist']
        country = d['country']

        if "N/A" in d['location']:
            location = None
        else:
            location = d['location']

        status = d['status']

        if "N/A" in d['formed_in']:
            formed_in = None
        else:
            formed_in = d['formed_in']

        if "N/A" in d['years_active'].strip():
            years_active = None
        else:
            years_active = d['years_active'].strip()

        genre = d['genre']

        if "N/A" in d['lyrical_themes']:
            lyrical_themes = None
        else:
            lyrical_themes = d['lyrical_themes']

        results.append((n, artist, country, location, status,
                        formed_in, years_active, genre, lyrical_themes))

        n = n + 1

    return results


data = dedup('output.jl')
bands = clean_data(data)

con = sqlite3.connect('metal_archives.db')

with con:
    cur = con.cursor()

    cur.execute("DROP TABLE IF EXISTS bands")
    cur.execute(
        "CREATE TABLE bands(id INT, artist TEXT, country TEXT, location TEXT, status TEXT, formed_in INT, years_active TEXT, genre TEXT, lyrical_themes TEXT)")
    cur.executemany(
        "INSERT INTO bands VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", bands)
