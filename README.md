# Metal Archives Scrapper

I started this little scrapping project to play around with [Scrapy](https://scrapy.org/) and [Splash](https://github.com/scrapinghub/splash), while it worked just fine I realised it would be much easier ( and more portable ) to just get the Json data directly and rely on **lxml** for the entire job .

The scrapper grabs all the data from [The Encyclopedia Metallum](https://www.metal-archives.com) .

- Artist
- Country
- Location
- Status
- Formed in
- Years active
- Genre
- Lyrical themes

We first get all the band links into a csv file, and then we process every band link at a gentle *variable* rate to play nice with the server .

The data gets cleaned up and then ingested in a *sqlite db* for for now .